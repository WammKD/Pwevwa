#!/usr/bin/env dash

font="$1"
data=$(wget "https://wttr.in/?u&format=%c_%t_%C_%S_%z_%s_%f_%w_%h_%p_%P_%u_%m_%M" 2>/dev/null -O - | sed 's/+//g')
icon=$(case $(echo $data | cut -d_ -f1 | tr -d '[:blank:]') in
       	"⛅️")
       		echo $(case $(date '+%H') in
       		       	0[0-6]|19|2[0-9])
       		       		if
       		       			[ $font ]
       		       		then
       		       			echo ""
       		       		else
       		       			echo "weather-few-clouds-night-symbolic.symbolic.png"
       		       		fi
       		       		;;
       		       	*)
       		       		if
       		       			[ $font ]
       		       		then
       		       			echo ""
       		       		else
       		       			echo "weather-few-clouds-symbolic.symbolic.png"
       		       		fi
       		       		;;
       		       esac)
       		;;
       	"🌫")
       		if
       			[ $font ]
       		then
       			echo ""
       		else
       			echo "weather-fog-symbolic.symbolic.png"
       		fi
       		;;
       	"🌦")
       		if
       			[ $font ]
       		then
       			echo ""
       		else
       			echo "weather-showers-scattered-symbolic.symbolic.png"
       		fi
       		;;
       	"☀️")
       		echo $(case $(date '+%H') in
       		       	0[0-6]|19|2[0-9])
       		       		if
       		       			[ $font ]
       		       		then
       		       			echo ""
       		       		else
       		       			echo "weather-clear-night-symbolic.symbolic.png"
       		       		fi
       		       		;;
       		       	*)
       		       		if
       		       			[ $font ]
       		       		then
       		       			echo ""
       		       		else
       		       			echo "weather-clear-symbolic.symbolic.png"
       		       		fi
       		       		;;
       		       esac)
       		;;
       	"☁️")
       		if
       			[ $font ]
       		then
       			echo ""
       		else
       			echo "weather-overcast-symbolic.symbolic.png"
       		fi
       		;;
       	"⛈"|"🌧")
       		if
       			[ $font ]
       		then
       			echo ""
       		else
       			echo "weather-showers-symbolic.symbolic.png"
       		fi
       		;;
       	"🌩")
       		if
       			[ $font ]
       		then
       			echo ""
       		else
       			echo "weather-storm-symbolic.symbolic.png"
       		fi
       		;;
       	"❄️"|"🌨")
       		if
       			[ $font ]
       		then
       			echo ""
       		else
       			echo "weather-snow-symbolic.symbolic.png"
       		fi
       		;;
       	*)
       		if
       			[ $font ]
       		then
       			echo ""
       		else
       			echo "computer-fail-symbolic.symbolic.png"
       		fi
       		;;
       esac)
text=$(result=$(echo $data | cut -d_ -f2)

       if
       	[ ${#result} -gt 6 ]
       then
       	echo "--°F"
       else
       	echo $result
       fi)

rm wget-log*

if
	[ $font ]
then
	# waybar custom module
	tooltip="<b>$(echo $data | cut -d_ -f3)</b>\r\
\r\
 $(echo $data | cut -d_ -f4 | { read gmt ; date +"%I:%M:%S %p" -d "$gmt" ; }) \
 $(echo $data | cut -d_ -f5 | { read gmt ; date +"%I:%M:%S %p" -d "$gmt" ; }) \
 $(echo $data | cut -d_ -f6 | { read gmt ; date +"%I:%M:%S %p" -d "$gmt" ; })\r\
\r\
<i>Feels like:</i>\t<b>$(echo $data | cut -d_ -f7)</b>\r\
<i>Wind:</i>\t\t<b>$(echo $data | cut -d_ -f8)</b>\r\
<i>Humidity:</i>\t<b>$(echo $data | cut -d_ -f9)</b>\r\
<i>Precipitation:</i>\t<b>$(echo $data | cut -d_ -f10)</b>\r\
<i>Pressure:</i>\t<b>$(echo $data | cut -d_ -f11)</b>\r\
<i>UV Index:</i>\t<b>$(echo $data | cut -d_ -f12)</b>\r\
<i>Moon Phase:</i>\t<b>$(echo $data | cut -d_ -f13)</b>"

	echo "{ \"text\":    \"$icon $text\", \
          \"tooltip\": \"$tooltip\"    }"
else
	# tint2 executor
	echo "$(echo "$XDG_DATA_DIRS" | tr ':' '\n' | while read path
	                                              do
	                                              	if
	                                              		[ -f "$path/icons/Diminye-Icons/status/512/$icon" ]
	                                              	then
	                                              		echo "$path"

	                                              		break
	                                              	fi
	                                              done)/icons/Diminye-Icons/status/512/$icon"
	echo "$text"
fi
